package ADAPTER;

public class GameAccount {
    public static void main(String[] args) {
        IInternet connection = new WifiAdapter(new Internet());
        connection.connect();
        connection.disconnect();
    }
}