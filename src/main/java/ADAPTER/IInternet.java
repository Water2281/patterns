package ADAPTER;

public interface IInternet {
    void connect();
    void disconnect();
}
