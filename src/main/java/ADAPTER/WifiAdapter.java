package ADAPTER;

public class WifiAdapter implements IInternet {
    private Internet internet;

    public WifiAdapter(Internet internet){
        this.internet=internet;
    }

    @Override
    public void connect() {
        internet.Connect();
    }

    @Override
    public void disconnect() {
        internet.Disconnect();
    }
}
