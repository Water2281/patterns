package ADAPTER;

public class Internet {
    public void Connect(){
        System.out.println("Successful connection!");
    }

    public void Disconnect(){
        System.out.println("Successful disconnection!");
    }
}
