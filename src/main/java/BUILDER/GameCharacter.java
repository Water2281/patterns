package BUILDER;

public class GameCharacter {
    private String name;
    private Role type;
    private int level;
    private Weapon weapon;

    public GameCharacter(GameCharacterBuilder gameCharacterBuilder){
        this.name = gameCharacterBuilder.getName();
        this.type = gameCharacterBuilder.getType();
        this.level = gameCharacterBuilder.getLevel();
        this.weapon = gameCharacterBuilder.getWeapon();
    }

    public static class GameCharacterBuilder {
        private String name;
        private Role type;
        private int level;
        private Weapon weapon;

        public GameCharacterBuilder(String name, Role type) {
            this.name = name;
            this.type = type;
        }

        public GameCharacterBuilder LevelOf(int l){
            if(l<=25) {
                this.level = l;
                return this;
            }
            this.level = 25;
            return this;
        }

        public GameCharacterBuilder WeaponOf(Weapon w){
            this.weapon=weapon;
            return this;
        }

        public String getName() {
            return name;
        }

        public Role getType() {
            return type;
        }

        public int getLevel() {
            return level;
        }

        public Weapon getWeapon() {
            return weapon;
        }

        public GameCharacter build(){
            return new GameCharacter(this);
        }
    }


}
