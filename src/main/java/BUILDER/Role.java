package BUILDER;

public enum Role {
    Carry,
    Support,
    Nuker,
    Durable,
    Initiator,
    Disabler,
    Pusher,
}
