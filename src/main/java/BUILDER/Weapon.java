package BUILDER;

public enum Weapon {
    Magic,
    Sword,
    Axe,
    Rapier,
    Knifes,
}
