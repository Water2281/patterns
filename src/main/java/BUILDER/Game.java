package BUILDER;

public class Game {
    public static void main(String[] args) {
        GameCharacter gameCharacter = new GameCharacter
                .GameCharacterBuilder("Invoker",Role.Nuker)
                .LevelOf(25)
                .WeaponOf(Weapon.Magic)
                .build();
    }
}
