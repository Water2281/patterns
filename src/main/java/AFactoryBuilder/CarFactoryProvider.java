package AFactoryBuilder;

import AFactoryBuilder.Components.CarType;
import AFactoryBuilder.Components.Color;
import AFactoryBuilder.Components.Control;
import AFactoryBuilder.Components.Model;
import Type.LuxuryCar;

public class CarFactoryProvider {
    public Car getCar(CarType t, Model m, Color c, int e, Control con) {
            return new Car
                    .CarBuilder(m).Type(t)
                    .Color(c).Control(con).EngineVolume(e)
                    .build();
    }
}
