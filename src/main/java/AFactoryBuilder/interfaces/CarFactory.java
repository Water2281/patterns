package AFactoryBuilder.interfaces;

public interface CarFactory {
    void getCar();
}
