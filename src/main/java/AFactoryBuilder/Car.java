package AFactoryBuilder;

import AFactoryBuilder.Components.CarType;
import AFactoryBuilder.Components.Color;
import AFactoryBuilder.Components.Control;
import AFactoryBuilder.Components.Model;

public class Car extends AbstractCar {

    public Car(CarBuilder carBuilder) {
        super.model = carBuilder.getModel();
        super.color = carBuilder.getColor();
        super.engineVol = carBuilder.getEngineVol();
        super.control = carBuilder.getControl();
        super.type = carBuilder.getType();
    }

    public static class CarBuilder{
        private Model model;
        private Color color;
        private int engineVol;
        private Control control;
        private CarType type;

        public CarBuilder(Model model) {
            this.model = model;
        }

        public Model getModel() {
            return model;
        }

        public Color getColor() {
            return color;
        }

        public int getEngineVol() {
            return engineVol;
        }

        public Control getControl() {
            return control;
        }

        public CarType getType() { return type; }

        public CarBuilder Type(CarType t){
            this.type = t;
            return this;
        }

        public CarBuilder Color(Color c){
            this.color = c;
            return this;
        }

        public CarBuilder EngineVolume(int e){
            this.engineVol = e;
            return this;
        }

        public CarBuilder Control(Control c){
            this.control = c;
            return this;
        }

        public Car build(){
            return new Car(this);
        }
    }


}
