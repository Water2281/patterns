package AFactoryBuilder.Components;

public enum Model {
    Rio,
    Cerato,
    Optima,
    Kia
}
