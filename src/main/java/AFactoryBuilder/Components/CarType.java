package AFactoryBuilder.Components;

public enum CarType {
    Luxury,
    Small,
    Sedan
}
