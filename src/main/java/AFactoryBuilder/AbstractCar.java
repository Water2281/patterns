package AFactoryBuilder;

import AFactoryBuilder.Components.CarType;
import AFactoryBuilder.Components.Color;
import AFactoryBuilder.Components.Control;
import AFactoryBuilder.Components.Model;

public abstract class AbstractCar {
     protected Model model;
     protected Color color;
     protected int engineVol;
     protected Control control;
     protected CarType type;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getEngineVol() {
        return engineVol;
    }

    public void setEngineVol(int engineVol) {
        this.engineVol = engineVol;
    }

    public Control getControl() {
        return control;
    }

    public void setControl(Control control) {
        this.control = control;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }
}
